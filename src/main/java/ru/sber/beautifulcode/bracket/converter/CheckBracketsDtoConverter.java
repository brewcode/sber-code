package ru.sber.beautifulcode.bracket.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import ru.sber.beautifulcode.bracket.model.CheckBracketsResponseDto;

import static java.util.Objects.requireNonNullElse;

/**
 * Используется и автоматически добавляется в контекст в рамках {@link org.springframework.core.convert.ConversionService}
 */
final class CheckBracketsDtoConverter implements Converter<Boolean, CheckBracketsResponseDto> {

    @Override
    public CheckBracketsResponseDto convert(@Nullable Boolean source) {

        return new CheckBracketsResponseDto(requireNonNullElse(source, false));
    }
}