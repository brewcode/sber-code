package ru.sber.beautifulcode.bracket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
class SberBracketApplication {

    public static void main(String[] args) {

        SpringApplication.run(SberBracketApplication.class, args);
    }
}
