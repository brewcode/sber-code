package ru.sber.beautifulcode.bracket.support;


import jakarta.annotation.Nullable;

import java.util.Arrays;

import static java.util.Arrays.binarySearch;
import static org.apache.commons.lang3.BooleanUtils.negate;

/**
 * Класс final так как логика завязаны на приватные методы.<br>
 * Если нужно изменить реализацию, то нужно создавать новую {@link CheckBracketsTextProcessor}<br>
 * Описание смотри в {@link #hasCorrectBrackets(String)}<br>
 *
 * @see #hasCorrectBrackets
 */
record CheckBracketsTextProcessorImpl(char openBracket, char closeBracket, char[] emptyChars) implements CheckBracketsTextProcessor {

    //// RECORD INIT ////

    CheckBracketsTextProcessorImpl {

        var sortedEmptyChars = Arrays.copyOf(emptyChars, emptyChars.length);
        Arrays.sort(sortedEmptyChars);
        emptyChars = sortedEmptyChars;
    }

    //// STATIC FIELDS ////

    private static final char OPEN_BRACKET = '(';

    private static final char CLOSE_BRACKET = ')';

    // https://habr.com/ru/articles/23250/
    private static final char[] EMPTY_CHARS = new char[]{0x0020, 0x00A0, 0x2003, 0x2002, 0x2004, 0x2005, 0x2006, 0x2009, 0x200A};

    public static final CheckBracketsTextProcessorImpl DEFAULT = new CheckBracketsTextProcessorImpl(
        OPEN_BRACKET,
        CLOSE_BRACKET,
        EMPTY_CHARS
    );

    //// PUBLIC METHODS ////

    /**
     * Проверка корректности расположения скобок.
     * А также дополнительное требование, что в скобках обязательно должен быть текст.
     * Текст находящийся во вложенных скобках распространяется на родительские скобки и считается, что в них тоже есть текст.
     * <p>
     * Открывающий символ задается в {@link #openBracket}<br>
     * Закрывающий символ задается в {@link #closeBracket}<br>
     * Закрывающий символ задается в {@link #closeBracket}<br>
     * <p>
     * Если скобок нет, то это true.<br>
     * Если строка пустая или null, то true.<br>
     * Все валидации входных данных на других уровнях<br>
     * Этот метод максимально толерантен к любым входным данным.
     *
     * @param text Текст на проверку корректности расположения скобок.
     * @return true - скобки расположены корректно и в них есть текст либо строка не содержит скобок<br>
     * false - нехватает открывающих или закрывающих скобок, или в них отсутствует текст.
     */
    public boolean hasCorrectBrackets(@Nullable String text) {

        // Если нет скобок, то считаем, что строка корректная в общем случае (валидация входных данных на других уровне)
        if (text == null || text.isBlank())
            return true;

        int openedBrackets = 0;
        boolean hasTextBetweenBrackets = false;

        for (char candidateChar : text.toCharArray()) {

            if (isOpenBracket(candidateChar)) {
                openedBrackets++;

            } else if (isCloseBracket(candidateChar)) {
                openedBrackets--;

                // Если закрытых скобок больше чем открытых, то сразу false
                if (openedBrackets <= -1)
                    return false;

                // Если после закрытия скобки текста с момента открытия не было обнаружено, то false
                if (negate(hasTextBetweenBrackets))
                    return false;

                // Если все текущие открытые скобки закрылись, то сбрасываем флаг наличия текста для будущих пар скобок
                if (isAllBracketClosed(openedBrackets))
                    hasTextBetweenBrackets = false;

            } else {
                // Если найден хотя бы один текстовый символ при обходе в состоянии не закрытой скобки,
                // значить внутри всех открытых текущих пар есть текст
                hasTextBetweenBrackets |= isTextChar(candidateChar);
            }
        }

        return isAllBracketClosed(openedBrackets);
    }

    //// PRIVATE METHODS ////

    private boolean isAllBracketClosed(int openedBrackets) {

        return openedBrackets == 0;
    }

    private boolean isOpenBracket(char candidateChar) {

        return candidateChar == openBracket;
    }

    private boolean isCloseBracket(char candidateChar) {

        return candidateChar == closeBracket;
    }

    private boolean isTextChar(char candidateChar) {
        // Arrays.binarySearch быстрее чем Set.contains особенно в сортированном массиве
        return binarySearch(emptyChars, candidateChar) <= -1;
    }
}
