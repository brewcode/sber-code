package ru.sber.beautifulcode.bracket.support;

import jakarta.annotation.Nullable;

public interface CheckBracketsTextProcessor {

    /**
     * Проверка корректности расположения скобок.
     *
     * @param text Текст на проверку корректности расположения скобок.
     * @return true - скобки расположены корректно и в них есть текст либо строка не содержит скобок<br>
     * false - нехватает открывающих или закрывающих скобок, или в них отсутствует текст.
     */
    boolean hasCorrectBrackets(@Nullable String text);
}
