package ru.sber.beautifulcode.bracket.support;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootConfiguration
class SupportSpringBootConfiguration {

    @Bean
    public CheckBracketsTextProcessor checkBracketsTextProcessor() {

        return CheckBracketsTextProcessorImpl.DEFAULT;
    }
}
