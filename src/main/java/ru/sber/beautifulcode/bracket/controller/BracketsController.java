package ru.sber.beautifulcode.bracket.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import ru.sber.beautifulcode.bracket.api.CheckBracketsApi;
import ru.sber.beautifulcode.bracket.model.CheckBracketsRequestDto;
import ru.sber.beautifulcode.bracket.model.CheckBracketsResponseDto;
import ru.sber.beautifulcode.bracket.service.BracketsService;

@RestController
@RequiredArgsConstructor
class BracketsController implements CheckBracketsApi {

    private final BracketsService bracketsService;
    private final ConversionService conversionService;

    //// PUBLIC METHODS ////

    @Override
    public Mono<CheckBracketsResponseDto> checkBrackets(Mono<CheckBracketsRequestDto> checkBracketsRequestDto, ServerWebExchange exchange) {

        return checkBracketsRequestDto
            .map(CheckBracketsRequestDto::getText)
            .map(bracketsService::checkText)
            .map(text -> conversionService.convert(text, CheckBracketsResponseDto.class));
    }
}
