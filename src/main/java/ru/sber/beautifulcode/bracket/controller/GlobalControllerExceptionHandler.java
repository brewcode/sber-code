package ru.sber.beautifulcode.bracket.controller;

import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;

import java.util.Arrays;
import java.util.Collection;

import static java.util.Objects.requireNonNullElse;

@RestControllerAdvice
final class GlobalControllerExceptionHandler {

    @ExceptionHandler(WebExchangeBindException.class)
    public ProblemDetail validationOrBindingExceptionHandler(WebExchangeBindException ex) {

        final Collection<String> errors = Arrays
            .stream(requireNonNullElse(ex.getDetailMessageArguments(), new Object[]{}))
            .filter(GlobalControllerExceptionHandler::isEmptyCollection)
            .map(Object::toString)
            .toList();

        final ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(ex.getStatusCode(), "Validation or Binding error");
        problemDetail.setProperty("errors", errors);

        return problemDetail;
    }

    //// PRIVATE METHODS ////

    private static Boolean isEmptyCollection(Object candidate) {

        return (candidate instanceof Collection<?>)
            && !((Collection<?>) candidate).isEmpty();
    }
}