package ru.sber.beautifulcode.bracket.service;

import org.springframework.stereotype.Service;
import ru.sber.beautifulcode.bracket.support.CheckBracketsTextProcessor;

@Service
record BracketsServiceImpl(CheckBracketsTextProcessor checkBracketsTextProcessor) implements BracketsService {

    @Override
    public boolean checkText(String text) {

        return checkBracketsTextProcessor.hasCorrectBrackets(text);
    }
}