package ru.sber.beautifulcode.bracket.controller;

import lombok.experimental.UtilityClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import ru.sber.beautifulcode.bracket.model.CheckBracketsRequestDto;
import ru.sber.beautifulcode.bracket.model.CheckBracketsResponseDto;
import ru.sber.beautifulcode.bracket.model.ErrorResponseDto;

import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class BracketsControllerE2ETest {

    @LocalServerPort
    private int serverPort;

    private WebTestClient client;

    @BeforeEach
    void beforeAll() {

        client = WebTestClient
            .bindToServer()
            .baseUrl("http://localhost:%s".formatted(serverPort))
            .build();
    }

    //// TEST METHODS ////

    //////// TEST 1

    @ParameterizedTest
    @ValueSource(strings = TestData.SUCCESSFUL_INPUT_TEXT)
    void checkBracketsShouldReturnTrue(String inputText) {

        client.post()
            .uri("/api/checkBrackets")
            .bodyValue(new CheckBracketsRequestDto(inputText))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectAll(
                spec -> spec.expectStatus().isOk(),
                spec -> spec.expectHeader().contentType(MediaType.APPLICATION_JSON),
                spec -> spec.expectBody(CheckBracketsResponseDto.class)
                    .isEqualTo(new CheckBracketsResponseDto(true))
            );
    }

    //////// TEST 2

    @ParameterizedTest
    @ValueSource(strings = TestData.UNSUCCESSFUL_INPUT_TEXT)
    void checkBracketsShouldReturnFalse(String inputText) {

        client.post()
            .uri("/api/checkBrackets")
            .bodyValue(new CheckBracketsRequestDto(inputText))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectAll(
                spec -> spec.expectStatus().isOk(),
                spec -> spec.expectHeader().contentType(MediaType.APPLICATION_JSON),
                spec -> spec.expectBody(CheckBracketsResponseDto.class)
                    .isEqualTo(new CheckBracketsResponseDto(false))
            );
    }

    //////// TEST 3

    @ParameterizedTest
    @CsvSource(nullValues = "null", delimiterString = "|", textBlock = """
        null | [text: 'must not be null']
        ''   | [text: 'size must be between 1 and 2147483647']
        """)
    void checkBracketsShouldReturnBadRequest(String inputText, String expectedError) {

        client.post()
            .uri("/api/checkBrackets")
            .bodyValue(new CheckBracketsRequestDto(inputText))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectAll(
                spec -> spec.expectStatus().isBadRequest(),
                spec -> spec.expectHeader().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                spec -> spec.expectBody(ErrorResponseDto.class)
                    .isEqualTo(new ErrorResponseDto()
                        .title("Bad Request")
                        .status(400)
                        .type("about:blank")
                        .detail("Validation or Binding error")
                        .instance("/api/checkBrackets")
                        .errors(List.of(expectedError))
                    )
            );
    }

    //// TEST DATA ////

    @UtilityClass
    private static class TestData {

        private static final String SUCCESSFUL_INPUT_TEXT = """
            Вчера я отправился в поход в лес (это мое любимое место для отдыха) вместе с друзьями.
                        Мы выбрали маршрут, который проходил через горные потоки и поля (для разнообразия).
                        В начале пути погода была отличной, солнце светило ярко, и птицы радостно пели.
                        Однако, когда мы подошли ближе к вершине горы, небо стало покрываться облаками,
                        (как будто природа готовила нам небольшой сюрприз).
                        Несмотря на это, виды были захватывающими, особенно когда мы достигли высшей точки и
                        увидели прекрасный вид на долину (я почувствовал, что все усилия стоили того).
            """;

        private static final String UNSUCCESSFUL_INPUT_TEXT = SUCCESSFUL_INPUT_TEXT + ')';
    }
}
