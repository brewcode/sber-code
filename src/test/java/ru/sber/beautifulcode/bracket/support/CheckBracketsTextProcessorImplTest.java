package ru.sber.beautifulcode.bracket.support;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CheckBracketsTextProcessorImplTest {

    private final CheckBracketsTextProcessorImpl checkBracketsTextProcessorImpl = CheckBracketsTextProcessorImpl.DEFAULT;

    //// TEST METHODS ////

    //////// TEST 1

    @DisplayName("hasCorrectBrackets должен вернуть TRUE, если передан текст с корректной расстановкой скобок или без них")
    @ParameterizedTest
    @CsvSource(nullValues = "null", textBlock = """
        text ( text )( ((text)) (text) )
        (text)
        (text(text)text)
        text
        '   '
        ''
        null
        """)
    void hasCorrectBracketsShouldReturnTrueOnCorrectText(String text) {
        Assertions
            .assertThat(checkBracketsTextProcessorImpl.hasCorrectBrackets(text))
            .isTrue();
    }

    //////// TEST 2

    @DisplayName("hasCorrectBrackets должен вернуть FALSE, если передан текст с некорректной расстановкой скобок или без них")
    @ParameterizedTest
    @CsvSource(textBlock = """
        ()
        ( )
        text(
        text)
        ((text)
        (text))
        (text)( )
        (()(text))(text)
        (text(text)text()
        text ( text )( (text)text) (text) ))
        """)
    void hasCorrectBracketsShouldReturnFalseOnIncorrectText(String text) {
        Assertions
            .assertThat(checkBracketsTextProcessorImpl.hasCorrectBrackets(text))
            .isFalse();
    }
}
