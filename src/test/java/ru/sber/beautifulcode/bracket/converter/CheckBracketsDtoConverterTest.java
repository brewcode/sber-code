package ru.sber.beautifulcode.bracket.converter;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.core.convert.converter.Converter;
import ru.sber.beautifulcode.bracket.model.CheckBracketsResponseDto;

import java.util.stream.Stream;

class CheckBracketsDtoConverterTest {

    private static final Converter<Boolean, CheckBracketsResponseDto> checkBracketsDtoConverter = new CheckBracketsDtoConverter();

    //// TEST METHODS ////

    //////// TEST 1

    @ParameterizedTest
    @MethodSource
    void shouldConvertAnyBooleanValues(Boolean input, CheckBracketsResponseDto expected) {

        Assertions.assertThat(checkBracketsDtoConverter.convert(input))
            .isEqualTo(expected);
    }

    //// TEST DATA ////

    private static Stream<Arguments> shouldConvertAnyBooleanValues() {

        return Stream.of(
            Arguments.of(true, new CheckBracketsResponseDto(true)),
            Arguments.of(false, new CheckBracketsResponseDto(false)),
            Arguments.of(null, new CheckBracketsResponseDto(false))
        );
    }
}
