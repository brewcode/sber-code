sber-code
---

## Описание

Web сервис, которые состоит из одного REST ресурса:
- `/checkBrackets`

Сервис поможет людям понять, правильно ли они расставили скобки в своем тексте.
И, казалось бы, задача простая и известная - проверить открывающие и закрывающие пары.
Но вот в чем соль: мы хотим, чтобы между этими скобками всегда был какой-то текст.
То есть пустые скобки у нас тут не пройдут, они считаются некорректной штукой.

## Особенности реализации

- Java 17
- Spring Boot WebFlux v3.1.3
- Openapi v3 спецификация
- Сборщик Gradle v8.3

> Основная особенность, что для описания API используется OpenAPI в папке `openapi/sber-code.yml`
> Код генерируется `Gradle` плагином `org.openapi.generator`
> Сгенерированный во время компиляции код помещается в папку `build/generated`
> Он будет добавлен в `sourceSet` для разработки и `classpath` при запуске

## Запуск

- Установить `Java 17`
- У машины должен быть доступ до `https://repo.maven.apache.org/maven2/` и `https://plugins.gradle.org/m2/`
- Скачать распаковать проект в папку
- Запустить
  - Для `Linux`
      ```
      ./gradlew bootRun
      ```
  - Для `Windows cmd`
      ```
      gradlew.bat bootRun
      ```
- Сервер стартует по адресу [localhost:8080/api](http://localhost:8080/api)
- Документация `API Swagger` находится по адресу [localhost:8080/api/swagger-ui.html](http://localhost:8080/api/swagger-ui.html)

## Ресурсы

- [SWAGGER-UI](http://localhost:8080/api/swagger-ui.html)

## Советы

Проверка актуальности версий зависимостей:
```shell
gradle dependencyUpdates
```

Генерация OpenApi кода (запускается при компиляции автоматически):
```shell
gradle openApiGenerate
```